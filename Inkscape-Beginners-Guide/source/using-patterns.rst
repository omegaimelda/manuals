**************
Using Patterns
**************

Inkscape comes with a couple of preset patterns. To apply them to an
object, you need to:

#. select the object on the canvas
#. open the :guilabel:`Fill and Stroke` dialog, and in the :guilabel:`Fill` (or
   :guilabel:`Stroke paint`) tab, click on the icon for patterns |image0|
#. You will then see a dropdown menu with many pattern names. Select any pattern
   from the list to apply it immediately to the object.

.. figure:: images/pattern_polka_dots_medium_with_pattern_list.png
    :alt: Polka dots pattern with pattern list
    :class: screenshot

    Object with stock pattern 'Polka dots, medium' and a view of the
    list of available patterns

.. |image0| image:: images/paint_pattern_icon.png
